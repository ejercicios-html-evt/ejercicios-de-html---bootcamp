let canvas = document.querySelector("#myCanvas");
let ctx = canvas.getContext("2d");
ctx.fillStyle = "red";
ctx.fillRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);

let listaFrames = [];
let spriteX = 0;
let spriteY = 0;
let spriteIndex = 0;

const inicializarFrames = () => {
    for (let i=0; i<7; i++){
        let myImage = new Image();
        myImage.src = `/animation-frames/Frame${i}.png`;
        if(i==6){
            myImage.addEventListener('load', pintarImagen);
        }
        listaFrames.push(myImage);
    }
}

// console.log(listaFrames);

//reconozco la tecla pulsada por su código para avanzar o retroceder en la lista de frames
document.addEventListener('keydown', function(event) {
    if(event.keyCode == '39'){
        pasarFrame(1);
    }
    else
    {
        if(event.keyCode == '37'){
            pasarFrame(-1);
            return;
        }
    }
});

//al soltar la tecla vuelve al frame 6 que es el que está parado
document.addEventListener('keyup', function(e) {
    if (e.keyCode == '37' || e.keyCode == '39') {
        ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
        spriteIndex = 6;

        ctx.fillStyle = "red";
        ctx.fillRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
        ctx.drawImage(listaFrames[spriteIndex], spriteX, spriteY, listaFrames[spriteIndex].width, listaFrames[spriteIndex].height);
    }

});

function pintarImagen(event){
    ctx.drawImage(event.target, 0, 0);
}

function pasarFrame(direccion){
    if ( direccion > 0){
        spriteIndex = ( spriteIndex >= 6 ) ? 0 : ++spriteIndex;
        if ( spriteX >= 1200 ){
            spriteX = 1200;
            girar();
        }else{
            spriteX += 5;
        }
    }  else {
        spriteIndex = ( spriteIndex <= 0 ) ? 6 : --spriteIndex;
        spriteX = ( spriteX <= 0 ) ? 0 : spriteX - 5;
    }
    ctx.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);

    ctx.fillStyle = "red";
    ctx.fillRect(0, 0, canvas.offsetWidth,canvas.offsetHeight);
    ctx.drawImage(listaFrames[spriteIndex], spriteX, spriteY, listaFrames[spriteIndex].width, listaFrames[spriteIndex].height);
}

function girar(){
    ctx.save();
    ctx.translate(canvas.offsetWidth, 0);
    ctx.scale(-1,1);
    ctx.drawImage(canvas, 0, 0);
    ctx.restore();
}

inicializarFrames();

ctx.fillStyle = "red";
ctx.fillRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);