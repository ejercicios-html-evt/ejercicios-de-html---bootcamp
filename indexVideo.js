var canvas = document.querySelector("#myCanvas");
var video = document.querySelector("video");
var btnCaptura = document.querySelector("#capture");
var btnBlancoNegro = document.querySelector("#monotone");
var btnClear = document.querySelector("#reset");
var btnBlur = document.querySelector("#blur");
btnCaptura.addEventListener("click", capturar);
btnBlancoNegro.addEventListener("click", monotone);
btnBlur.addEventListener("click", blur);
btnClear.addEventListener("click", borrar);

var ctx = canvas.getContext("2d");

function capturar(){
    // borrar();
    ctx.fillRect(0, 0, video.offsetWidth, video.offsetHeight);
    ctx.drawImage(video, 0, 0, video.offsetWidth, video.offsetHeight);
}

function borrar(){
    ctx.clearRect(0, 0, video.offsetWidth, video.offsetHeight);
}

function monotone(){
    // borrar();
    // capturar();
    let imgData1=ctx.getImageData(0,0,canvas.width,canvas.height);
    let data1=imgData1.data;

    for(let i = 0; i < data1.length; i += 4) {
      let grayscale= data1[i]+data1[i+1]+data1[i+2]/3;
      data1[i]=grayscale;
      data1[i+1]=grayscale;
      data1[i+2]=grayscale;
    }

    ctx.putImageData(imgData1,0,0);
}

function blur(){
    // borrar();
    // capturar();
    ctx.globalAlpha = 0.3;

    let offset = 3;

    for (let i=1; i<=8; i+=1) {
        ctx.drawImage(canvas, offset, 0, canvas.width - offset, canvas.height, 0, 0, canvas.width-offset, canvas.height);
        ctx.drawImage(canvas, 0, offset, canvas.width, canvas.height - offset, 0, 0,canvas.width, canvas.height-offset);
    }
}